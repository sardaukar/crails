require "uri"

module Crails
  class Request
    getter params, method, path

    def initialize @method, @path, @params ; end

    def self.from_http req : HTTP::Request
      uri = URI.parse(req.path)
      params = build_params(uri.query)
      Request.new req.method, uri.path || "/", params
    end

    private def self.build_params query_string
      params = {} of String => String

      return params unless query_string

      query_string.split("&").each do |param|
        k, v = param.split("=")
        params[k] = v
      end

      params
    end

  end
end
