module Crails
  class HTTPHandler < ::HTTP::Handler

    def initialize @routes ; end

    def call http_request : HTTP::Request
      request = Request.from_http(http_request)

      route = match_routes request

      if route
        HTTP::Response.new(200, route.proc.call(request) )
      else
        HTTP::Response.new(404, "")
      end
    end

    private def match_routes req : Request
      @routes.find { |route| route.match?(req) }
    end

  end
end
