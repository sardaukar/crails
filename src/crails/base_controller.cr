module Crails
  abstract class BaseController
    def initialize @request
      @params = @request.params
    end
  end
end
