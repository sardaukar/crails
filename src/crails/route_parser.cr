module Crails
  class RouteParser

    def initialize @app ; end

    def eval
      with DSL.new(@app) yield
    end

    struct DSL

      def initialize @app ; end

      def routes
        with self yield
      end

      def matcher(method, path, &proc : Request -> String)
        @app.routes << Route.new(method, path, proc)
      end

      macro match(path, target, method)
        matcher({{method}}, {{path}}) do |request|
          controller = {{target.split('#').first.capitalize.id}}Controller.new(request)
          controller.{{target.split('#').last.id}}
        end
      end

      macro resources(resource)
        match "{{resource.id}}",      "{{resource.id}}#index",   :get
        match "{{resource.id}}/new",  "{{resource.id}}#new",     :get
        match "{{resource.id}}",      "{{resource.id}}#create",  :post
        match "{{resource.id}}/edit", "{{resource.id}}#edit",    :get
        match "{{resource.id}}/:id",  "{{resource.id}}#update",  :put
        match "{{resource.id}}/:id",  "{{resource.id}}#show",    :get
        match "{{resource.id}}/:id",  "{{resource.id}}#destroy", :delete
      end

      macro root(target)
        matcher("get", "/") do |request|
          controller = {{target.split('#').first.capitalize.id}}Controller.new(request)
          controller.{{target.split('#').last.id}}
        end
      end

      {% for method in %w(get post put delete patch) %}
        macro {{method.id}}(path, hash)
          matcher("{{method.id}}", \{{path}}) do |request|
            controller = \{{hash[:to].split("#").first.capitalize.id}}Controller.new(request)
            controller.\{{hash[:to].split("#").last.id}}
          end
        end
      {% end %}

    end

  end
end
