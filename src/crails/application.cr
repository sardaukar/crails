require "http"

module Crails
  class Application
    property routes

    def initialize
      @routes = [] of Route
    end

    def boot!
      puts "booting CRAILS..."
      load_routes
      puts "now open for business on port 9001!"
      listen!
    end

    def listen! port=9001
      server = HTTP::Server.new port, HTTPHandler.new(@routes)
      server.listen
    end

    def self.config
      @@config ||= Config.new
    end

    private def load_routes
      if File.exists?("config/routes.cr")
        RouteParser.new(self).eval do
          {{ `cat config/routes.cr` }}
        end
      else
        puts "routes not found, bailing!"
        exit 0
      end
    end

  end
end
