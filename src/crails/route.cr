require "regex"

module Crails
  class Route
    getter proc

    def initialize @method, @path, @proc ; end

    def match? req : Request
      if req.method == @method.upcase
        regex = Regex.new(@path.to_s.gsub(/(:\w*)/, ".*"))

        return false if req.path.split("/").length != @path.split("/").length

        if req.path.match(regex)
          true
        else
          false
        end
      else
        false
      end
    end

  end
end
